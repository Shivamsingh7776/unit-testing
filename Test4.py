import unittest
import Task4
import csv
from csv import DictReader

file = open('mock_deliveries.csv','r')
Match_data = DictReader(file)

Output_data = {'TS Mills':3,'Kg Rabada':5}

class TestTask4(unittest.TestCase):
    def test_Calculate(self):
         self.assertEqual(Task4.Calculate(Match_data),Output_data)
        

        
if __name__ == '__main__':
    unittest.main()
