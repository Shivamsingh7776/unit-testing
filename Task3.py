import csv
import matplotlib.pyplot as plt
from csv import DictReader
with open('mock_deliveries.csv', 'w', newline='') as file:    # With -> Contaxt manneger
    The_writer = csv.writer(file)
    The_writer.writerow(['match_id', 'inning', 'batting_team',
                        'bowling_team', 'extra_runs', 'total_runs', 'Bowler', 'Year'])
    The_writer.writerow(['1', '1', 'Sunrisers Hyderabad',
                        'Royal Challengers Bangalore', '2', '2', 'TS Mills', '2015'])
    The_writer.writerow(['1', '1', 'Sunrisers Hyderabad',
                        'Royal Challengers Bangalore', '1', '1', 'TS Mills', '2015'])
    The_writer.writerow(['2', '1', 'Royal Challengers Bangalore',
                        'Delhi Daredevils', '1', '1', 'A Nehra', '2013'])
    The_writer.writerow(['2', '1', 'Royal Challengers Bangalore',
                        'Delhi Daredevils', '1', '1', 'A Nehra', '2013'])
    The_writer.writerow(['3', '1', 'Gujarat Lions',
                        'Kolkata Knight Riders', '0', '2', 'TA Boult', '2016'])
    The_writer.writerow(['3', '1', 'Gujarat Lions',
                        'Kolkata Knight Riders', '2', '3', 'TA Boult', '2016'])
    The_writer.writerow(['4', '1', 'Mumbai Indians',
                        'Delhi Daredevils', '1', '2', 'Kg Rabada', '2015'])
    The_writer.writerow(['4', '1', 'Mumbai Indians',
                        'Delhi Daredevils', '2', '3', 'Kg Rabada', '2015'])
    The_writer.writerow(['5', '1', 'Chennai Super Kings',
                        'Punjab Kings', '3', '4', 'Arshdip', '2020'])
    The_writer.writerow(['5', '1', 'Chennai Super Kings',
                        'Punjab Kings', '1', '4', 'Arshdip', '2020'])
    The_writer.writerow(['6', '1', 'Rising Pune Supergiants',
                        'Deccan Chargers', '2', '2', 'Shane Watson', '2009'])
    The_writer.writerow(['6', '1', 'Rising Pune Supergiants',
                        'Deccan Chargers', '2', '3', 'Shane Watson', '2009'])
    The_writer.writerow(['7', '1', 'Kolkata Knight Riders',
                        'Royal Challengers Bangalore', '3', '4', 'Md Siraj', '2016'])
    The_writer.writerow(['7', '1', 'Kolkata Knight Riders',
                        'Royal Challengers Bangalore', '2', '3', 'Md Siraj', '2016'])
    The_writer.writerow(['8', '1', 'Punjab Kings',
                        'Sunrisers Hyderabad', '2', '3', 'Js Holder', '2019'])
    The_writer.writerow(['8', '1', 'Punjab Kings',
                        'Sunrisers Hyderabad', '1', '3', 'Js Holder', '2019'])


def Calculate(Match_data):
    Extra_Run = {}
    for row in Match_data:
        if int(row['Year']) == 2016:
            if row['bowling_team'] not in Extra_Run:
                Extra_Run[row['bowling_team']] = int(row['extra_runs'])
            else:
                Extra_Run[row['bowling_team']] += int(row['extra_runs'])
    print(Extra_Run)
    return Extra_Run


def Transfer(Extra_Run):
    Extra_Run_Team = list(Extra_Run.values())
    Bowling_Team = list(Extra_Run.keys())
    return Extra_Run_Team, Bowling_Team


def Graph(Extra_Run, Bowling_Team):
    plt.bar(Bowling_Team, Extra_Run)
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()


def main():
    file = open('mock_deliveries.csv', 'r')
    Match_data = DictReader(file)

    Extra_Run = Calculate(Match_data)
    Exatra_Run_per_team, Bowling_Team = Transfer(Extra_Run)

    Graph(Exatra_Run_per_team, Bowling_Team)


if __name__ == '__main__':
    main()