import csv
import matplotlib.pyplot as plt

from csv import DictReader

def Calculate(Match_Data):

    Match_Won = {}

    for row in Match_Data:
        if row['season'] not in Match_Won:
            Match_Won[row['season']] = row['winner']
        else:
            Match_Won[row['season']] += row['winner']
    print(Match_Won)            
    return Match_Won

def Transfer(Match_Won):
    Year_Data = list(Match_Won.values())
    Team_Name = list(Match_Won.keys())

    return Year_Data,Team_Name

def Graph(Year_Data,Team_Name):
    plt.bar(Year_Data,Team_Name)
    plt.xticks(rotation = 90)
    plt.tight_layout()
    plt.show()

def main():
    Csv_Read = open('mock_matches.csv','r')
    Match_data = DictReader(Csv_Read)
   
    Match_Won = Calculate(Match_data)
    
    Year_data,Team_name = Transfer(Match_Won)


    Graph(Year_data,Team_name)
    



    


if __name__ == '__main__':
    main()