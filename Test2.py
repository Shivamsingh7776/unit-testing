import unittest
import Task2
import csv
from csv import DictReader

file = open('mock_matches.csv','r')
Match_data = DictReader(file)
dict = {}
for row in Match_data:
    if row['season'] not in dict:
        dict[row['season']] = row['winner']
    else:
        dict[row['season']] += row['winner']

Match_Won_data = {'2017':'Sunrisers Hydrabad','2008':'Royal Challengers Bangalore','2009':'Chennai Super Kings','2010':'Mumbai Indians','2012':'Deccan Chargers','2015':'Rajasthan RoyalsChennai Super Kings','2016':'Pune Warriors IndiaDeccan Chargers'}



class TestTask2(unittest.TestCase):
    def test_calculate(self):
        self.assertEqual(Task2.Calculate(Match_data),Match_Won_data)


if __name__ == '__main__':
    unittest.main()