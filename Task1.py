# def add(x,y):
#     return x + y
# print(add(12,34))
import csv
import matplotlib.pyplot as plt
from csv import DictReader
with open('mock_matches.csv','w',newline='') as file:
    The_writer = csv.writer(file)
    The_writer.writerow(['id','season','city','date','team1','team2','winner'])
    The_writer.writerow(['1','2017','Hydrabad','2017-04-05','Sunrisers Hydrabad','Royal Challengers Bangalore','Sunrisers Hydrabad'])
    The_writer.writerow(['2','2008','Bangalore','2008-04-18','Kolkata knight Riders','Royal Challengers Bangalore','Royal Challengers Bangalore'])
    The_writer.writerow(['3','2009','Cape Town','2009-04-18','Mumbai Indains','Chennai Super Kings','Chennai Super Kings'])
    The_writer.writerow(['4','2010','Mumbai','2010-03-13','Mumbai Indians','Rajasthan Royals','Mumbai Indians'])
    The_writer.writerow(['5','2012','Jaipur','2012-04-17','Deccan Chargers','Rajasthan Royals','Deccan Chargers'])
    The_writer.writerow(['6','2015','Jaipur','2015-04-17','Deccan Chargers','Rajasthan Royals','Deccan Chargers'])
    The_writer.writerow(['7','2015','Jaipur','2015-04-18','Deccan Chargers','Rajasthan Royals','Deccan Chargers'])
    The_writer.writerow(['8','2016','Jaipur','2016-04-17','Deccan Chargers','Rajasthan Royals','Deccan Chargers'])
    The_writer.writerow(['9','2016','Jaipur','2016-04-18','Deccan Chargers','Rajasthan Royals','Deccan Chargers'])


def Calculate(Match_data):
    Total_Matches = {}

    for row in  Match_data:
        if row['season'] not in Total_Matches:
            Total_Matches[row['season']] = 1
        else:
            Total_Matches[row['season']] += 1
    return Total_Matches


def Transfer(Total_Match):
    Year_Data = list(Total_Match.keys())
    Number_Of_Match = list(Total_Match.values())

    return Year_Data,Number_Of_Match

def Graph(Year_Data,Number_Of_Match):
    plt.bar(Year_Data,Number_Of_Match)
    plt.show()


def main():
    file_data = open('mock_matches.csv','r')
    Match_Data = DictReader(file_data)
    Total_match = Calculate(Match_Data)
    Year_Data,Number_Of_Match = Transfer(Total_match)
    Graph(Year_Data,Number_Of_Match)


if __name__ == '__main__':
    main()

#mock_matches.csv