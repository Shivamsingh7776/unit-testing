import csv
import matplotlib.pylab as plt

from csv import DictReader


def Calculate(Deliveries_Data):
    Top_Eco = {}

    for row in Deliveries_Data:
        if int(row['Year']) == 2015:
            if row['Bowler'] not in Top_Eco:
                Top_Eco[row['Bowler']] = int(row['total_runs'])
            else:
                Top_Eco[row['Bowler']] += int(row['total_runs'])
        Top_Eco_Bow = dict(sorted(Top_Eco.items(),key=lambda x:x[1],reverse=0))

    temp = {}
    count = 0
    for key,value in Top_Eco_Bow.items():
        temp[key] = value
        if count > 2:
            break
        count += 1
    print(temp)
    return temp

def Transfer(Top_Eco):
    Bowler_Name = list(Top_Eco.keys())
    Run_Con = list(Top_Eco.values())

    return Bowler_Name,Run_Con

def Graph(Bowler_Name,Top_Eco_Bow):
    plt.bar(Bowler_Name,Top_Eco_Bow)
    plt.show()


def main():
    file = open('mock_deliveries.csv', 'r')
    Deliveries_data = DictReader(file)
    Top_Eco = Calculate(Deliveries_data)

    Bowler_Name, Top_Eco_Bow = Transfer(Top_Eco)
    Graph(Bowler_Name, Top_Eco_Bow)

if __name__ == '__main__':
    main()